#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);



    connect(&tim, &QTimer::timeout, this, &MainWindow::slot_TimerOut);
    tim.setInterval(700);
    //tim.start();

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(slot_clickPB1()));
    connect(ui->pushButton, &QPushButton::clicked, this, &MainWindow::slot_clickPB1);
    connect(ui->pushButton_2, &QPushButton::clicked, this, &MainWindow::slot_clickPB2);
    connect(ui->pushButton_3, &QPushButton::clicked, this, &MainWindow::slot_clickPB3);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slot_clickPB2()
{
    ui->pushButton_2->setText("T1");
}

void MainWindow::slot_clickPB3()
{
    ui->label->setText("Hi world... 你好。。。 привет");
}

void MainWindow::slot_clickPB1()
{
    static QString s, s_nospace;
    if(ui->lineEdit->isModified()){
        s = ui->lineEdit->displayText();
        s_nospace = "";
        for(int i = 0; i < s.length(); i++){
            if(s[i] != ' '){
                s_nospace += s[i];
            }
        }
        ui->label->setText(s_nospace);
        ui->lineEdit->setModified(false);
    }
}



void MainWindow::slot_TimerOut()
{
    static int num = 0;

    if (status.empty()) {
        ui->label->setText("empty");
    }

    if (num >= status.size()) {

        num = 0;
        return;
    }

    ui->label->setText(tr("status %1 : %2").arg(num).arg(status.at(num++)));
}

