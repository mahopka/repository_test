#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include    <QPushButton>
#include    <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QVector<int> status;
    QTimer tim;

    void CreateButton();

public slots :

    void slot_clickPB2();
    void slot_clickPB3();

    void slot_clickPB1();

    void slot_TimerOut();
};
#endif // MAINWINDOW_H
